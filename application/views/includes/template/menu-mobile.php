<!--Menu Mobile-->
<div class="menu-wrap page-scroll">
    <div class="main-menu">
        <!--<h4 class="title-menu">Main menu</h4>-->
        <button class="close-button" id="close-button">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <ul class="nav-menu">
        <!--<li class="selected active"><a href="#">Home</a>
            <ul class="child-nav dropdown-nav">
                <li><a href="index-v2.html">Home V2</a></li>
                <li><a href="index-onepage.html">One Page</a></li>
                <li><a href="index-layer.html">Layer Slider</a></li>
                <li><a href="index-shop.html">E-commerce</a></li>
                <li><a href="index-store.html">Store</a></li>
                <li><a href="index-sport-club.html">Sport Club</a></li>
                <li><a href="index-boxing.html">Boxing</a></li>
                <li><a href="index-white.html">Light version</a></li>
                <li><a href="coming-soon.html">Under Construction</a></li>
            </ul>
        </li>-->
        <li class="selected active"><a href="<?= site_url() ?>#home">Home</a></li>
        <li><a href="<?= site_url() ?>#instalacions">Instal•lacions</a></li>
        <li>
            <a href="#packs">Packs</a>
            <ul class="child-nav dropdown-nav">
                <li><a href="<?= site_url('p/packs-aniversaris') ?>">Aniversaris</a></li>
                <li><a href="<?= site_url('p/packs-empreses') ?>">Empreses</a></li>
                <li><a href="<?= site_url('p/packs-comiat-de-solter') ?>">Comiat de solter</a></li>
                <li><a href="<?= site_url('p/packs-club-esportius') ?>">Club Esportius</a></li>
                <li><a href="<?= site_url('p/packs-partit-unic') ?>">Partit Unic</a></li>
                <li><a href="<?= site_url('p/packs-patit-unic-pro') ?>">Patit Únic Pro</a></li>
            </ul>
        </li>
        <li><a href="<?= site_url('reserva') ?>">RESERVA</a></li>
        <li><a href="<?= site_url() ?>#agencia">Agència</a></li>
        <li><a href="<?= site_url('p/contacto') ?>">Contacte</a></li>
    </ul>
</div>
<!--Menu Mobile-->
