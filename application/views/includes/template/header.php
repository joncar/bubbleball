<!--Header-->
<header id="header" class="header header-container reveal">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-3 logo">
                <a href="<?= site_url() ?>"><img src="<?= base_url() ?>img/logo.png" alt=""/></a>
            </div>
            <div class="col-md-10 nav-container">
                <nav class="megamenu collapse navbar-collapse navbar-main-collapse navbar-right mainnav col-md-10" role="navigation">
                    <ul class="nav-menu navbar-nav">
                        <!--<li class="selected active"><a class="link-menu" href="#home">Home</a>
                            <ul class="child-nav dropdown-nav">
                                <li><a href="index-v2.html">Home V2</a></li>
                                <li><a href="index-onepage.html">One Page</a></li>
                                <li><a href="index-layer.html">Layer Slider</a></li>
                                <li><a href="index-shop.html">E-commerce</a></li>
                                <li><a href="index-store.html">Store</a></li>
                                <li><a href="index-sport-club.html">Sport Club</a></li>
                                <li><a href="index-boxing.html">Boxing</a></li>
                                <li><a href="index-white.html">Light version</a></li>
                                <li><a href="coming-soon.html">Under Construction</a></li>
                            </ul>
                        </li>-->
                        <li class="selected active"><a class="link-menu" href="<?= site_url() ?>#home">Home</a></li>
                        <li><a class="link-menu" href="<?= site_url() ?>#instalacions">Instal•lacions</a></li>
                        <li>
                            <a class="link-menu" href="#packs">Packs</a>
                            <ul class="child-nav dropdown-nav">
                                <li><a href="<?= site_url('p/packs-aniversaris') ?>">Aniversaris</a></li>
                                <li><a href="<?= site_url('p/packs-empreses') ?>">Empreses</a></li>
                                <li><a href="<?= site_url('p/packs-comiat-de-solter') ?>">Comiat de solter</a></li>
                                <li><a href="<?= site_url('p/packs-club-esportius') ?>">Club Esportius</a></li>
                                <li><a href="<?= site_url('p/packs-partit-unic') ?>">Partit Unic</a></li>
                                <li><a href="<?= site_url('p/packs-patit-unic-pro') ?>">Patit Únic Pro</a></li>
                            </ul>
                        </li>
                        <li><a class="link-menu" href="<?= site_url('reserva') ?>">RESERVA</a></li>
                        <!-- <li><a class="link-menu" href="<?= site_url() ?>#agencia">Agència</a></li> -->
                        <li><a class="link-menu" href="<?= site_url('p/contacto') ?>">Contacte</a></li>
                    </ul>
                </nav>							
            </div>
            <button class="menu-button" id="open-button"></button>
        </div>
    </div>
</header>
<!--End Header-->
