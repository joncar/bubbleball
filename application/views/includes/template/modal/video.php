<!-- Modal -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="width:100%">  
    <div style="width:100%; height:100%; background:rgba(0,0,0,.7); position:fixed"></div>
    <div class="modal-dialog"  style="width:80%">
        <button data-dismiss="modal" class="modal-close" type="button" style="float:right; background:transparent; border:0px;">
            <svg ratio="1" icon="close-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" height="14" width="14">
                <line y2="13" x2="13" y1="1" x1="1" stroke-width="1.1" stroke="#fff" fill="none"/>
                <line y2="13" x2="1" y1="1" x1="13" stroke-width="1.1" stroke="#fff" fill="none"/>
            </svg>
        </button>
        <iframe width="100%" height="506" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/GqkMCnHcl5g?rel=0&amp;showinfo=0" style="height: 506px;"></iframe>
   
    </div>
</div>
