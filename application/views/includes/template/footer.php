
<!--Footer-->
<footer class="page-footer">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 infomation">
                    <div class="copy-right">
                        <div class="footer-right">
                            <div class="line1">Copyright &copy; 2017<a href="#"> BubbleBall</a></div>
                            <div class="line2">Web by Hipo</div>
                        </div>
                    </div>
                    <div class="social_icon">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 location">							
                    <div class="footer-title">
                        <h4>On estem</h4>			
                    </div>
                    <div class="address">
                        <p>Carrer Girona, 34<br>08700 IGUALADA. Barcelona<br>Tel : 693 805 834<br><a href="mailto:info@bubbleball.cat">Email : info@bubbleball.cat</a></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 send-mail">							
                    <div class="footer-title">
                        <h4>Suscriu-te a la newsletter</h4>			
                    </div>
                    <form name="" onsubmit="return subscribir()" method="post"  id="send-mail">												
                        <div class="info">Si vols estar informat de les nostre pomocions, events i noticies de Bubble Ball, dona't d'alta</div>						
                        <div class="email">
                            <div id="subsmessage"></div>
                            <input type="email" title="E-mail" name="email" id="emailSub" class="inputbox" placeholder="El teu email">
                            <button class="button" title="Submit" type="submit">
                                <i class="fa fa-arrow-right"></i>
                            </button>
                        </div>
                    </form>
                </div>						
            </div>
        </div>
    </section>
</footer>
<!--End Footer-->
<!--To Top-->
<div id="copyright">
    <div class="container">

        <div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

        <div class="clrDiv"></div>
    </div>
</div>
<!--End To Top-->
