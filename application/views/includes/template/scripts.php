<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBoW1mnkomGhsB2yL--AYoFdnE-jkgskSI&sensor=false&amp;language=es"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/shortcode-frontend.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.mixitup.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/classie.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/parallax/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/parallax/jquery.transform2d.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/parallax/script.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/parallax/parallax.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.scrollTo.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/wow.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/waypoints.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/masterslider.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/banner.js"></script>	
<script type="text/javascript" src="<?= base_url() ?>js/template/isotope.pkgd.min.js"></script>	
<script type="text/javascript" src="<?= base_url() ?>js/template/filtering.js"></script>		
<script type="text/javascript" src="<?= base_url() ?>js/template/home.map.js"></script>			
<script type="text/javascript" src="<?= base_url() ?>js/template/template.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/dropdown.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/theme.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/custom.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/main.js"></script>
<?php $this->load->view('predesign/datepicker'); ?>
<script>
$(".fecha").datetimepicker({
    timeFormat: "hh:mm",
    dateFormat: "dd/mm/yy",
    showButtonPanel: true,
    changeMonth: true,
    changeYear: true
});
$(".datetime-input-clear").button();
$(".datetime-input-clear").click(function(){
        $(this).parent().find(".datetime-input").val("");
        return false;
});	
</script>
<script>
function subscribir(){
  $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:$("#emailSub").val()},function(data){
      $("#subsmessage").html(data);
      $("#subsmessage").show();
  });
  return false;
}
</script>
<script>
function contactar(f){
  var form = new FormData(f);
  $.ajax({
      url:'<?= base_url('paginas/frontend/contacto') ?>',
      data:form,
      processData:false,
      type:'post',
      cache: false,
      contentType: false,
      success:function(data){
        $("#Conmessage").html(data);
        $("#Conmessage").show();
      }
  });
  return false;
}
</script>
