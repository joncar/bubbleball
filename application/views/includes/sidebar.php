<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(                        
                        'invitaciones'=>array('admin/invitaciones','admin/importar'),
                        'reservas'=>array('admin/reservas'),
                        'notificaciones'=>array('admin/notificaciones','admin/subscritos'),
                        'b'=>array('blog_categorias','blog'),                        
                        'paginas'=>array('admin/paginas','admin/subscriptores','admin/galeria_categorias'),                       
                        'seguridad'=>array('grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(      
                        'notificaciones'=>array('Notificaciones','fa fa-bullhorn'),
                        'b'=>array('Blog','fa fa-book'),
                        'galeria_categorias'=>array('Categoria de galeria'),
                        'invitaciones'=>array('Invitaciones','fa fa-address-card-o'),
                        'paginas'=>array('Paginas','fa fa-file-powerpoint-o'),                        
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#222222; font-size:8px; text-align:center">
            <a href="#" style="color:white;">EVA software</a>
        </div>
        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
