<!doctype html>
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
    <head>
        <title><?= !empty($title)?$title:'BubbleBall' ?></title>
        <!--meta info-->
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta name="description" content=""> 
        <link rel="stylesheet" href="<?= base_url() ?>css/template/style.css" type="text/css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/font-awesome.css" type="text/css">
        <!--[if !IE 9]><!-->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/effect.css" type="text/css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/animation.css" type="text/css">
        <!--<![endif]-->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/masterslider.css" type="text/css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/ms-fullscreen.css" type="text/css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>css/template/owl.carousel.css">
        <link rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>css/template/owl.transitions.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/color.css" type="text/css">        
        <?php if(!empty($scripts)): echo $scripts; else: ?>
        <script type="text/javascript" src="<?= base_url() ?>js/template/jquery-2.1.0.min.js"></script>
        <?php endif ?>
    </head> 
    <body id="page-top" class="" data-offset="90" data-target=".navbar-custom" data-spy="scroll">
        <div class="wrapper hide-main-content" id="home"> 
            <section  class="page one-page">
                <?php $this->load->view('includes/template/menu-mobile'); ?>
                <div class="content-wrapper">				
                    <div id="home">
                        <?php $this->load->view('includes/template/header'); ?>
                        <?php $this->load->view($view); ?>
                    </div>
                </div>
            </section>
            <?php $this->load->view('includes/template/footer'); ?>            
        </div>
        <?php $this->load->view('includes/template/scripts'); ?>
    </body>
</html>
