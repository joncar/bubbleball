<!--Banner-->
<section class="page-heading">
    <div class="title-slide">
        <div class="container">
            <div class="banner-content slide-container">									
                <div class="page-title">
                    <h3>Contactan's</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Banner-->
<div class="page-content">					
    <!-- Breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <ul>
                        <li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li><span>//</span></li>
                        <li class="category-2"><a href="#">Contacte</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->
    <!-- Our Team -->
    <section id="our-team" class="our-team-page">				
        <div class="our-team-head">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="headding">
                            <div class="headding-title">
                                <h4>Dades sobre nosltres</h4>
                                <div class="headding-bottom"></div>
                            </div>
                            <ul class="headding-content">
                                <li>
                                    <div class="icon-headding">
                                        <i class="fa fa-home"></i>
                                    </div>
                                    <div class="cont-headding">
                                        <h5>Carrer Girona, 34</h5>
                                        <p>08700 IGUALADA. BCN</p>
                                    </div>
                                </li>
                                  <br>
                                <li>
                                    <div class="icon-headding">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="cont-headding">
                                        <h5>Web </h5>
                                        <a href="http://bubbleball.cat">www.bubbleball.cat</a>
                                    </div>
                                </li>
                                  <br>
                                <li>
                                    <div class="icon-headding">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <div class="cont-headding">
                                        <h5>Telèfon </h5>
                                        <a href="tel:938030694">93 803 06 94</a><br>
                                        <a href="tel:693805834">69 380 58 34</a>
                            
                                        
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="contactInfo" class="col-md-8 col-sm-6 col-xs-12">
                        <div class="headding-title">
                            <h4>Nosaltres volen saber de vosaltres</h4>
                            <div class="headding-bottom"></div>
                        </div>
                        <div class="headding-content">
                            <p>Si tens qualsevol dubte pots omplir el formulari i us atendrem per ajudar-vos amb el que faci falta.
                            </p>
                            <p>
                                També ens pots trucar al <span>693 805 834</span> o enviar-nos un email omplint el formulari d'aquesta plana.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Team -->
    <!-- Contact Form -->
    <section class="contact-form">
        <div class="contact-submit">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="contact">
                            <h4>Formulari de contacte</h4>
                            <div class="headding-bottom"></div>
                            <form onsubmit="return contactar(this)" method="post">
                                <div class="form-group col-md-12">
                                    <input type="text" name="nom" class="control" required="required" placeholder="Nom complert">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="email" name="email" class="control" required="required" placeholder="Email">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" name="telefon" class="control" required="required" placeholder="Telèfon">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" name="poblacio" class="control" required="required" placeholder="Població">
                                </div>                                
                                <div class="row" style="margin-left:0px; margin-right:0px">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group col-md-12">
                                            Com t'arribat l'oferta?
                                            <ul class="list">
                                                <li><input type='radio' name='oferta' value='Flaix Fm'> Flaix Fm</li>
                                                <li><input type='radio' name='oferta' value='Radio FlaixBax'> Radio FlaixBax</li>
                                                <li><input type='radio' name='oferta' value='Prensa'> Premsa</li>
                                                <li><input type='radio' name='oferta' value='Tv'> Tv</li>
                                                <li><input type='radio' name='oferta' value='Recomendacion'> Recomanació d'un amic</li>
                                                <li><input type='radio' name='oferta' value='Otros'> Altres</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group col-md-12">
                                            <textarea name="texto" id="message" required="required" class="control" rows="8" placeholder="Comentari"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="Conmessage">
                                        
                                    </div>
                                </div>
                                <div class="form-group form-submit col-md-12">
                                    <button type="submit" name="submit" class="btn-submit">Enviar missatge</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="map-frame-event">
                    <input type="hidden" class="map-zoom" value="15" />
                    <input type="hidden" class="map-lat" value="41.5859946" />
                    <input type="hidden" class="map-lng" value="1.6237343000000237" />
                    <input type="hidden" class="map-icon-title" value="Bubbleball" />
                    <input type="visible" class="map-icon-img" value="<?= base_url("img/pin.png"); ?>" />
                </div>
    </section>
    <!-- End Contact Form -->
    <!-- Our Partners -->
    <!--<section class="our-partners">
        <div class="container">
            <div class="row">
                <div class="headding-title">
                    <h4>Els nostres col•laboradors</h4>
                    <div class="headding-bottom"></div>
                </div>
                <div class="brand">
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-02.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-03.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-04.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-05.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-06.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-02.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Partners -->
</div>
