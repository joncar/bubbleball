<!--Banner-->
<section class="slide-container to-top">
    <div class="ms-fullscreen-template" id="slider1-wrapper">
        <!-- masterslider -->
        <div class="master-slider ms-skin-default" id="masterslider-index">
            <div class="ms-slide slide-1" data-delay="0">
                <div class="slide-pattern"></div>
                <img src="<?= base_url() ?>img/blank.gif" data-src="<?= base_url() ?>img/bg_3.jpg" alt="lorem ipsum dolor sit"/>
                <h3 class="ms-layer hps-title1" style="left:95px"
                    data-type="text"
                    data-ease="easeOutExpo"
                    data-delay="1000"
                    data-duration="0"
                    data-effect="skewleft(30,80)">
                    DISFRUTA d'una
                </h3>																												
                <h3 class="ms-layer hps-title3" style="left:91px"
                    data-type="text"
                    data-delay="1900"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,40,t)"
                    data-ease="easeOutExpo">
                    EXPERIÈNCIA ÚNICA
                </h3>

                <h3 class="ms-layer hps-title4" style="left:95px"
                    data-type="text"
                    data-delay="2500"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,18,t)"
                    data-ease="easeOutExpo">
                    PARTICIPA
                </h3>
            </div>
            <div class="ms-slide slide-2" data-delay="0">
                <div class="slide-pattern"></div>							  
                <img src="<?= base_url() ?>img/blank.gif" data-src="<?= base_url() ?>img/bg_2.jpg" alt="lorem ipsum dolor sit"/>
                <h3 class="ms-layer hps-title1" style="left:95px"
                    data-type="text"
                    data-ease="easeOutExpo"
                    data-delay="1000"
                    data-duration="0"
                    data-effect="skewleft(30,80)">
                    DISFRUTA D'UNA 
                </h3>																												
                <h3 class="ms-layer hps-title3" style="left:91px"
                    data-type="text"
                    data-delay="1900"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,40,t)"
                    data-ease="easeOutExpo">
                    EXPERIÈNCIA ÚNICA
                </h3>

                <h3 class="ms-layer hps-title4" style="left:95px"
                    data-type="text"
                    data-delay="2500"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,18,t)"
                    data-ease="easeOutExpo">
                    PARTICIPA
                </h3>
            </div>	

            <div class="ms-slide slide-3" data-delay="0">
                <div class="slide-pattern"></div>							  
                <img src="<?= base_url() ?>img/blank.gif" data-src="<?= base_url() ?>img/bg-home-v2.jpg" alt="lorem ipsum dolor sit"/>
                <h3 class="ms-layer hps-title1" style="left:95px"
                    data-type="text"
                    data-ease="easeOutExpo"
                    data-delay="1000"
                    data-duration="0"
                    data-effect="skewleft(30,80)"
                    >
                    DISFRUTA D'UNA 
                </h3>																												
                <h3 class="ms-layer hps-title3" style="left:91px"
                    data-type="text"
                    data-delay="1900"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,40,t)"
                    data-ease="easeOutExpo"
                    >
                    D'UNA EXPERIÈNCIA ÚNICA
                </h3>

                <h3 class="ms-layer hps-title4" style="left:95px"
                    data-type="text"
                    data-delay="2500"
                    data-duration="0"
                    data-effect="rotate3dtop(-100,0,0,18,t)"
                    data-ease="easeOutExpo"
                    >
                    PARTICIPA
                </h3>
            </div>
            <!--div class="ms-slide slide-4">
               <div class="slide-pattern"></div>
                    <video data-autopause="false" data-mute="true" data-loop="true" data-fill-mode="fill">
                            <source src="<?= base_url() ?>img/video/demo.mp4" type="video/mp4"/>									
                    </video>  
            </div-->
        </div>
        <!-- end of masterslider -->
        <div class="to-bottom" id="to-bottom"><i class="fa fa-angle-down"></i></div>
    </div>
</section>
<!--End Banner-->


<div class="contents-main">
    <div id="ourteam">
        <!-- Our Team -->
        <section id="our-team" class="our-team-page">				
            <div class="our-team">
                <div class="our-team-content">
                    <div class="our-team-inner">							
                        <div class="custom">
                            <div class="our-team-tabs our-team-bottom our-team-fit  content-our-team" data-active="1">
                                <div class="our-team-panes">
                                    <div class="our-team-pane our-team-clear">
                                        <div class="our-team-img col-md-4 col-sm-4 col-xs-12">
                                            <img src="<?= base_url() ?>img/img_tabBlock4.jpg" alt="" data-mce-src="<?= base_url() ?>img/img_tabBlock4.jpg"/>
                                        </div>
                                        <div class="detail-our-team col-md-8 col-sm-8 col-xs-12">
                                            <div class="detail-our-team-inner">
                                                <div class="detail-our-team-desc">Som un empresa Igualadina que estem especialitzats en l’organització d’esdeveniments amb les famoses Bubbles! <br>Ens encarreguem de tot, el nostre staff us farà les explicacions dels jocs que fem i a gaudir d’un magnífic dia!</div>
                                                <!--<div class="detail-our-team-user">Bubbleball</div>-->
                                                <div class="detail-our-team-user"><a href="<?= site_url('reserva') ?>" style="color:black">FES LA TEVA RESERVA</a></div>
                                                <div class="detail-our-team-pos">Team</div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="our-team-pane our-team-clear">
                                        <div class="our-team-img col-md-4 col-sm-4 col-xs-12">
                                            <img src="<?= base_url() ?>img/img_tabBlock4_2.jpg" alt="" data-mce-src="<?= base_url() ?>img/img_tabBlock4_2.jpg" />
                                        </div>
                                        <div class="detail-our-team col-md-8 col-sm-8 col-xs-12">
                                            <div class="detail-our-team-inner">
                                                <div class="detail-our-team-desc">És un estil de joc desenfadat per a tots els públics. És molt fàcil! Només cal encabir-te dins la bombolla i intentar xutar la pilota esquivant els teus ribals i buscant els teus companys d'equip. Una forma divertida i original de jugar a futbol.</div>
                                                <div class="detail-our-team-user"><a href="<?= site_url('reserva') ?>" style="color:black">FES LA TEVA RESERVA</a></div>
                                                <div class="detail-our-team-pos">Team</div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="our-team-pane our-team-clear">
                                        <div class="our-team-img col-md-4 col-sm-4 col-xs-12">
                                            <img src="<?= base_url() ?>img/img_tabBlock4_3.jpg" alt="" data-mce-src="<?= base_url() ?>img/img_tabBlock4_3.jpg" />
                                        </div>
                                        <div class="detail-our-team col-md-8 col-sm-8 col-xs-12">
                                            <div class="detail-our-team-inner">
                                                <div class="detail-our-team-desc">Disposem de vàries instal·lacions destinades al Bubbleball, preparades per practicar aquest esport, amb tots els serveis necessaris. A més, el nostre staff estarà en tot moment allà encarregant-se que vosaltres només penseu en gaudir</div>                                                
                                                <div class="detail-our-team-user"><a href="<?= site_url('reserva') ?>" style="color:black">FES LA TEVA RESERVA</a></div>
                                                <div class="detail-our-team-pos">Team</div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="our-team-nav">
                                    <span class="" style="width: 33.333333333333%;">
                                        <span class='our-team-name'>QUI SOM</span>
                                        <span class='our-team-position'>BUBBLEBALL?</span>
                                    </span>
                                    <span class="yoga-trainer" style="width: 33.333333333333%;">
                                        <a href="<?= site_url('reserva') ?>">
                                            <span class='our-team-name'>COM PUC JUGAR</span>
                                            <span class='our-team-position'>RESERVA</span>
                                        </a>
                                    </span>
                                    <span class="boxing-trainer" style="width: 33.333333333333%;">
                                        <span class='our-team-name'>ELS PARTITS </span>
                                        <span class='our-team-position'>ON ES JUGUEN?</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Our Team -->		
    </div>
    <!-- Athlete Facts -->
    <section class="facts uk-section uk-section-large uk-light">
        <div class="background-overlay"style="background: none repeat scroll 0 0 rgba(21, 180, 188, 0);"></div>
        <div class="container" style="position:relative; z-index:2">                
            <div data-uk-scrollspy="target: &gt; .tw-heading; cls:uk-animation-slide-bottom-small; delay: 300">
                <div class="tw-element tw-heading uk-text-center uk-scrollspy-inview uk-animation-slide-bottom-small" style="">
                    <h5 style="font-size: 18px; opacity: 0.8" class="tw-sub-title">
                        UN COP D'ULL
                    </h5>
                    <h3 style=" font-size: 30px;">Mira el nostre vídeo</h3>
                    <button class="uk-margin-top tw-video-icon" data-target="#videoModal" data-toggle="modal" style="background-color: rgb(132, 211, 212); padding-top: 9px; padding-left: 15px;">
                        <i style="color: rgb(255, 255, 255); font-size: 38px;" class="fa fa-play"></i>
                    </button>
                    <!-- This is the modal with the default close button -->

                </div>
            </div>                
        </div>
    </section>
    <?php $this->load->view('includes/template/modal/video'); ?>




    <!--Collection-->
    <div class="collection"  id="packs">
        <div class="title-page">
            <h3 class="module-title-h3">tots els PACKS</h3>

        </div>	
        <div class="container">

            <div class="row">	

                <div class="collection-content">


                    <div class="col-md-6 col-sm-12 col-xs-12 box">											
                        <div class="women-content">
                            <div class="masonry-small">
                                <div class="price-table-img">
                                    <img alt="" src="<?= base_url() ?>img/newproducts/images-33.png">
                                </div>
                                <div class="price-table-text">
                                    <h3>Pack</h3>
                                    <h2>Partit Únic</h2>
                                    <div class="border-bottom"></div>
                                    <p>Gaudiu d'una hora de partit emocionant amb els amics, familiars, companys.... Tindreu beguda durant el partit!</p>
                                    <span>Des de <span> 19,90€</span> <a href="<?= site_url('p/packs-partit-unic') ?>">+INFO</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-sm-12 col-xs-12 box">											
                        <div class="women-content">
                            <div class="masonry-small">
                                <div class="price-table-img">
                                    <img alt="" src="<?= base_url() ?>img/newproducts/images-34.png">
                                </div>
                                <div class="price-table-text">
                                    <h3>Pack</h3>
                                    <h2>Partit Únic Pro</h2>
                                    <div class="border-bottom"></div>
                                    <p>Gaudiu de dues hores de partit emocionant amb els amics, familiars, companys... Tindreu beguda i pica-pica per recuperar forces!</p>
                                    <span>Des de <span> 34,90€</span> <a href="<?= site_url('p/packs-patit-unic-pro') ?>">+INFO</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 box">											
                        <div class="women-content">
                            <div class="masonry-small">
                                <div class="price-table-img">
                                    <img alt="" src="<?= base_url() ?>img/newproducts/images-3.png">
                                </div>
                                <div class="price-table-text">
                                    <h3>Pack</h3>
                                    <h2>aniversaris</h2>
                                    <div class="border-bottom"></div>
                                    <p>Gaudeix del dia amb el millor pack per al teu aniversari!<br> Tenim mini-bubbles i adult-bubbles així que tots hi podreu participar.</p>
                                    <span>Des de <span> 15,00€</span> <a href="<?= site_url('p/packs-aniversaris') ?>">+INFO</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 box">											
                        <div class="women-content">
                            <div class="masonry-small">
                                <div class="price-table-img">
                                    <img alt="" src="<?= base_url() ?>img/newproducts/images-55.png">
                                </div>
                                <div class="price-table-text">
                                    <h3>Pack</h3>
                                    <h2>Comiat de solter</h2>
                                    <div class="border-bottom"></div>
                                    <p>Ha arribat el dia! En una setmana et cases amb l'amor de la teva vida, i que millor que festejar-ho amb els teus amics i familiars.</p>
                                    <span>Des de<span> 19,90€</span> <a href="<?= site_url('p/packs-comiat-de-solter') ?>">+INFO</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 box">											
                        <div class="women-content">
                            <div class="masonry-small">
                                <div class="price-table-img">
                                    <img alt="" src="<?= base_url() ?>img/newproducts/images-2.png">
                                </div>
                                <div class="price-table-text">
                                    <h3>PACK</h3>
                                    <h2>Empreses</h2>
                                    <div class="border-bottom"></div>
                                    <p>Les dinàmiques de grup per a empreses són molt positives per cohesionar grups i enfortir equips.</p>
                                    <span>Des de <span> 19,90€</span> <a href="<?= site_url('p/packs-empreses') ?>">+INFO</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 box">											
                        <div class="women-content">
                            <div class="masonry-small">
                                <div class="price-table-img">
                                    <img alt="" src="<?= base_url() ?>img/newproducts/images-1.png">
                                </div>

                                <div class="price-table-text">
                                    <h3>Pack</h3>
                                    <h2>Clubs Esportius</h2>
                                    <div class="border-bottom"></div>
                                    <p>Aprofita per que el teu equip faci pinya, gaudeixi i practiquin<br> amb nous exercicis per millorar la seva motricitat!.</p>
                                    <span>Des de <span> 199€</span> <a href="<?= site_url('p/packs-club-esportius') ?>">+INFO</a></span>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
    <!--End Collection-->



    <!--Services-->		
    <section class="services">
        <div class="container">
            <div class="row">
                <div class="title-page">
                    <h4><span>BUBBLEBALL</span> Serveis</h4>
                </div>
                <div class="sevices-main">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="sevices-wapper">	
                            <div class="services-content">
                                <div class="intro-img">
                                    <img src="http://www.bubbleball.cat/img/icon-1.png" alt=""/>
                                </div>
                                <h4 class="services-title">Pistes</h4>
                                <hr class="border-title">
                                <h3 class="services-title">Gespa o indoor</h3>
                                <hr  class="border-title-1">
                                <div class="actions">
                                    <p>Localitzades a la comarca de l’Anoia. </p>
                                </div>										
                            </div>										
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="sevices-wapper">	
                            <div class="services-content">
                                <div class="intro-img">
                                    <img src="http://www.bubbleball.cat/img/icon-2.png" alt=""/>
                                </div>
                                <h4 class="services-title">Equipaments</h4>
                                <hr class="border-title">
                                <h3 class="services-title">Vestidors</h3>
                                <hr  class="border-title-1">
                                <div class="actions">
                                    <p>Per poder canviar-se abans i després de l’activitat.</p>
                                </div>	
                            </div>										
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="sevices-wapper">	
                            <div class="services-content">
                                <div class="intro-img">
                                    <img src="http://www.bubbleball.cat/img/icon-3.png" alt=""/>
                                </div>
                                <h4 class="services-title">Ambientació</h4>
                                <hr class="border-title">
                                <h3 class="services-title">Música, picapica</h3>
                                <hr  class="border-title-1">
                                <div class="actions">
                                    <p>Per gaudir d’un magnífic dia acompanyat dels teus amics i amigues.</p>
                                </div>	
                            </div>										
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="sevices-wapper">	
                            <div class="services-content">
                                <div class="intro-img">
                                    <img src="http://www.bubbleball.cat/img/icon-4.png" alt=""/>
                                </div>
                                <h4 class="services-title">Equipació</h4>
                                <hr class="border-title">
                                <h3 class="services-title">Bubbles X adults</h3>
                                <hr  class="border-title-1">
                                <div class="actions">
                                    <p>Bubbles de 1,50 / 1,70 m, completament equipades per començar a jugar.</p>
                                </div>	
                            </div>								
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="sevices-wapper">		
                            <div class="services-content">
                                <div class="intro-img">
                                    <img src="http://www.bubbleball.cat/img/icon-5.png" alt=""/>
                                </div>
                                <h4 class="services-title">Equipació</h4>
                                <hr class="border-title">
                                <h3 class="services-title">Bubbles infantils</h3>
                                <hr  class="border-title-1">
                                <div class="actions">
                                    <p>bubbles de 1,20 m, adaptades per a nens i nenes de  6 a 14 anys. </p>
                                </div>	
                            </div>										
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="sevices-wapper">	
                            <div class="services-content">
                                <div class="intro-img">
                                    <img src="http://www.bubbleball.cat/img/icon-6.png" alt=""/>
                                </div>
                                <h4 class="services-title">Seguretat</h4>
                                <hr class="border-title">
                                <h3 class="services-title">Assegurança</h3>
                                <hr  class="border-title-1">
                                <div class="actions">
                                    <p>La més complerta del mercat. </p>
                                </div>	
                            </div>										
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="sevices-wapper">		
                            <div class="services-content">
                                <div class="intro-img">
                                    <img src="http://www.bubbleball.cat/img/icon-7.png" alt=""/>
                                </div>
                                <h4 class="services-title">Personal</h4>
                                <hr class="border-title">
                                <h3 class="services-title">Staff</h3>
                                <hr  class="border-title-1">
                                <div class="actions">
                                    <p>Qui farà les explicacions del diferents jocs i guiarà les activitats.</p>
                                </div>	
                            </div>									
                        </div>									
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="sevices-wapper">	
                            <div class="services-content">
                                <div class="intro-img">
                                    <img src="http://www.bubbleball.cat/img/icon-8.png" alt=""/>
                                </div>
                                <h4 class="services-title">PAcks</h4>
                                <hr class="border-title">
                                <h3 class="services-title">6 tipus de partits</h3>
                                <hr  class="border-title-1">
                                <div class="actions">
                                    <p>Adpatats per a tots tipus de nivells-</p>
                                </div>	
                            </div>										
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Services-->
    <div id="class">
        <!-- Athlete Class -->
        <section id="classes" class="classes">
            <div class="classes-content">
                <div class="classes-wapper">
                    <div class="title-page">
                        <h3 class="module-title-h3">GALERIA DE FOTOS</h3>
                        <p>EL MILLORS PARTITS, AMB ELS MILLORS AMICS, GAUDEUX D'UNS DELS DIES ON MAI OBLIDADARÀS </p>
                    </div>
                </div>
                <div id="filters" class="filters button-group container">
                    <button  data-filter="*" class="filter button is-checked">Totes</button>
                    <?php
                    $categorias = $this->db->get('galeria_categorias');
                    foreach ($categorias->result() as $n => $g):
                        $categorias->row($n)->fotos = $this->db->get_where('galeria', array('galeria_categorias' => $g->id));
                        ?>
                        <button  data-filter=".<?= strtolower($g->nombre) ?>" class="filter button"><?= $g->nombre ?></button>                    									
<?php endforeach ?>
                </div>
                <div class="class-our">				
                    <div class="classes-athlete">
                        <div class="classes-inner">
                            <div id="filtering-demo">			
                                <section id="our-class-main" class="isotope">
                                    <?php foreach ($categorias->result() as $c): ?>
                                        <?php foreach ($c->fotos->result() as $f): ?>
                                            <div class="mix <?= strtolower($c->nombre) ?> element-item col-xs-12 col-sm-6 col-md-4" data-category="<?= strtolower($c->nombre) ?>">
                                                <div class="box-inner">
                                                    <img src="<?= base_url('img/galeria/' . $f->foto) ?>" alt=""/>
                                                    <div class="box-content">
                                                        <div class="table">
                                                            <div class="box-celle">
                                                                <span>VEURE</span>
                                                            </div>
                                                        </div>	
                                                    </div>
                                                </div>										
                                            </div>
                                        <?php endforeach ?>
                                    <?php endforeach ?>
                                </section>										
                            </div>
                        </div>
                    </div>
                </div>																		
            </div>
        </section>
        <!-- End Athlete Class -->


        <!--Price Table-->        
        <section class="price-table" id="instalacions" style=" background: url(../../img/transp_bg.png);">
            <div class="container">

                <div class="row">
                    <div class="title-page" style=" color: #333;">
                        <h3 class="module-title-h3">Instal•lacions</h3>
                        <p style=" font-size: 16px">Les millors instal·lacions indoor i outdoor de la comarca! </p>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="price-contents">
                            <div class="col-md-4 col-sm-12 col-xs-12" style="padding-left:15px;">
                                <div class="price-tb">
                                    <div class="col-md-12 price-table-content">
                                        <div class="price-table-img">
                                            <img alt="" src="<?= base_url() ?>img/newproducts/images-pilota.png">
                                        </div>
                                        <div class="price-table-text" style="height: 254px;left: 41%;overflow: hidden;position: relative;width: 61%;padding-top: 9%;font-size: 1.2em">

                                            <h2>PISTES</h2>
                                            <div class="border-bottom"></div>
                                            <p style=" font-size: 15px">Escull si prefereixes jugar a l'exterior i gaudir del sol o a la nostra pista interior!</p>
                                        </div>
                                    </div>
                                    <div class="col-md-12 price-list">
                                        <div class="price-table-1">
                                            <ul>
                                                <img alt="" src="<?= base_url() ?>img/newproducts/images-gespa.jpg">
                                                <li>2 pistes d'herba artificial</li>
                                            </ul>
                                            <ul>
                                                <img alt="" src="<?= base_url() ?>img/newproducts/images-indor.jpg">
                                                <li>1 pista de parquet flotant indoor</li>
                                            </ul>
                                            <ul>
                                                <img alt="" src="<?= base_url() ?>img/newproducts/images-vest.jpg">
                                                <li>Vestuaris per a tot l'equip</li>
                                            </ul>	
                                            <ul>
                                                <img alt="" src="<?= base_url() ?>img/newproducts/images-555.jpg">
                                                <li>Proteccions per a tots els assistents</li>
                                            </ul>	

                                        </div>
                                        <div class="plan">
                                            <a href="<?= site_url('reserva') ?>">RESERVA partit</a>
                                        </div>										
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="price-tb">
                                    <div class="col-md-12 price-table-content">
                                        <div class="price-table-img">
                                            <img alt="" src="<?= base_url() ?>img/newproducts/images-45.png">
                                        </div>
                                        <div class="price-table-text" style="height: 254px;left: 41%;overflow: hidden;position: relative;width: 61%;padding-top: 9%;font-size: 1.2em">

                                            <h2>EQUIPAMENTS</h2>
                                            <div class="border-bottom"></div>
                                            <p style=" font-size: 15px">No et preocupis de res, el nostre staff ve preparat amb tot el material</p>                                            
                                        </div>

                                    </div>                                
                                    <div class="col-md-12 price-list">
                                        <div class="price-table-1">
                                            <ul>
                                                <img src="<?= base_url() ?>img/newproducts/images-equ.jpg">
                                                <li>Bubble's per a viure l'experiència</li>
                                            </ul>
                                            <ul>
                                                <img src="<?= base_url() ?>img/newproducts/images-arb.jpg">
                                                <li>Staff per arbitrar els jocs</li>
                                            </ul>
                                            <ul>
                                                <img src="<?= base_url() ?>img/newproducts/images-modalitat.jpg">
                                                <li>Diferentes modalitats de jocs</li>
                                            </ul>	
                                            <ul>
                                                <img src="<?= base_url() ?>img/newproducts/images-kat.jpg">
                                                <li>Beguda i aperitius per al grup</li>
                                            </ul>	
                                        </div>
                                        <div class="plan">
                                            <a href="<?= site_url('reserva') ?>">RESERVA partit</a>
                                        </div>										
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="price-tb">
                                    <div class="col-md-12 price-table-content">
                                        <div class="price-table-img">
                                            <img alt="" src="<?= base_url() ?>img/newproducts/images-porquet.png">
                                        </div>
                                        <div class="price-table-text" style="height: 254px;left: 41%;overflow: hidden;position: relative;width: 61%;padding-top: 9%;font-size: 1.2em">

                                            <h2>OFERTES</h2>
                                            <div class="border-bottom"></div>
                                            <p style=" font-size: 15px">Aprofita les nostres ofertes exclusives per a grups</p>
                                        </div>
                                    </div>
                                    <div class="col-md-12 price-list">
                                        <div class="price-table-1">
                                            <ul>
                                                <img alt="" src="<?= base_url() ?>img/newproducts/images-emp.jpg">
                                                <li>Packs per a empreses</li>
                                            </ul>
                                            <ul>
                                                <img alt="" src="<?= base_url() ?>img/newproducts/images-de.jpg">
                                                <li>Packs per a clubs esportius</li>
                                            </ul>
                                            <ul>
                                                <img alt="" src="<?= base_url() ?>img/newproducts/images-an.jpg">
                                                <li>Packs per a aniversaris</li>
                                            </ul>
                                            <ul>
                                                <img alt="" src="<?= base_url() ?>img/newproducts/images-des.jpg">
                                                <li>Packs per a comiats</li>
                                            </ul>

                                        </div>
                                        <div class="plan">
                                            <a href="<?= site_url('reserva') ?>">RESERVA partit</a>
                                        </div>										
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Price Table-->

        <!--Modern Equipment-->
        <section class="training modern hidden-xs">
            <div class="wpb-wrapper">
                <!-- PARALLAX WINDOWS -->
                <div class="parallax-block-wrap-module">
                    <div class="parallax-block bt-advance-parallax" id="">
                        <!-- PARALLAX CONTENT -->
                        <div class="control-button">
                            <div class="nav-wrap hidden">
                                <div class="nav-wrap-in next">
                                    <span class="nav-next"></span>
                                </div>
                                <div class="nav-wrap-in prev">
                                    <span class="nav-prev"></span>
                                </div>
                            </div>
                            <div class="button-wrap">
                                <span class="button close-btn"></span>
                            </div>
                        </div>
                        <div class="parallax-background" style="background:url(<?= base_url() ?>/img/boxing/bg_44.jpg)">
                            <!--Textured and color-->
                            <div class="parallax-background-overlay"></div>
                            
                        </div>

                        <div class="parallax-block-content default-pos">
                            <div class="container">
                                <div class="parallax-gallery">
                                    <h1>ENS ENCARREGUEM DE TOT! </h1>
                                    <p class="join-us">El nostre staff us farà les explicacions del joc</p>                                
                                </div>
                                <div class="plan" style="width: 120px; border: 2px solid white; margin-top: 30px;">
                                    <a href="<?= base_url('reserva') ?>">RESERVA</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>													
        </section>				
        <!--End Modern Equipment-->        

    </div>

    <div id="contact">

        <!-- Contact Form -->
        <section class="contact-form">
            <div class="contact-submit">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="contact">
                                <h4>FORMULARI CONTACTE</h4>
                                <div class="headding-bottom"></div>
                                <form id="main-contact-form" class="main-contact-form row" name="contact-form" method="post">
                                    <div class="form-group col-md-12">
                                        <input type="text" name="nom complert" class="control" required="required" placeholder="Nom complert">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <input type="email" name="email" class="control" required="required" placeholder="Email">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <input type="text" name="telèfon" class="control" required="required" placeholder="Telèfon">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <input type="text" name="població" class="control" required="required" placeholder="Població">
                                    </div>                                
                                    <div class="row" style="margin-left:0px; margin-right:0px">
                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group col-md-12">
                                                Com t'arribat l'oferta?
                                                <ul class="list">
                                                    <li><input type='radio' name='oferta' value='Flaix Fm'> Flaix Fm</li>
                                                    <li><input type='radio' name='oferta' value='Radio FlaixBax'> Radio FlaixBax</li>
                                                    <li><input type='radio' name='oferta' value='Prensa'> Premsa</li>
                                                    <li><input type='radio' name='oferta' value='Tv'> Tv</li>
                                                    <li><input type='radio' name='oferta' value='Recomendacion'> Recomanació d'un amic</li>
                                                    <li><input type='radio' name='oferta' value='Otros'> Altres</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group col-md-12">
                                                <textarea name="texto" id="message" required="required" class="control" rows="8" placeholder="Comentari"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-submit col-md-12">
                                        <button type="submit" name="submit" class="btn-submit">Enviar missatge</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-map">
                <div class="map-frame-event">
                    <input type="hidden" class="map-zoom" value="15" />
                    <input type="hidden" class="map-lat" value="41.5859946" />
                    <input type="hidden" class="map-lng" value="1.6237343000000237" />
                    <input type="hidden" class="map-icon-title" value="Bubbleball" />
                    <input type="visible" class="map-icon-img" value="<?= base_url("img/pin.png"); ?>" />
                </div>

            </div>					
        </section>
        <!-- End Contact Form -->
    </div>
</div>
