<!--Banner-->
<section class="page-heading">
    <div class="title-slide">
        <div class="container">
            <div class="banner-content slide-container">									
                <div class="page-title">
                    <h3>Pack ANIVERSARIS</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Banner-->
<div class="page-content">					
    <!-- Breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li><span>//</span></li>
                <li class="category-1"><a href="#">Packs</a></li>
                <li><span>//</span></li>
                <li class="category-2"><a href="#">Pack ANIVERSARIS</a></li>
            </ul>
        </div>
    </div>
    <!-- End Breadcrumbs -->
    
    <!-- Main Content -->
    <div class="main-content class-detail" style=" background: url(../../img/transp_bg.png);">
        <div class="container">
            <div class="row">
                <div class="profile-details">
                    <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                        <section class="banner-details">
                            <div id="carousel" class="carousel slide" data-ride="carousel">											
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <img src="<?= base_url() ?>img/bg-trainer-profile.jpg" alt="">
                                    </div>

                                    <div class="item">
                                        <img src="<?= base_url() ?>img/bg-trainer-profile-2.jpg" alt="">
                                    </div>

                                    <div class="item">
                                        <img src="<?= base_url() ?>img/bg-trainer-profile-3.jpg" alt="">
                                    </div>													
                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">anterior</span>
                                </a>
                                <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Següent</span>
                                </a>
                            </div>
                        </section>											
                        <!-- End Banner Content -->

                        <!-- 
 Training Experience                        <div class="content-pages">
                            <section class="experience">
                                <div class="experience-content">
                                
                                    <div class="experience-title">
                                        <h5>Pack ANIVERSARIS</h5>													
                                    </div>
                                    <div class="experience-main experience-main-plus" data-active-first="yes" >
                                        <div class="experience-spoiler">
                                            <div class="experience-details-title">
                                                <i class="fa fa-plus"></i>
                                                Què inclou?
                                                <span class="experience-details-collapse"></span>
                                            </div>
                                            <div class="experience-details-content">
                                            • Gaudeix del dia amb el millor pack per al teu aniversari!<br>

											• El dia començarà amb un divertit partit i jocs de Bubble Football amb els assistents a la teva gran festa.<br>

											• Al finalitzar us prepararem un pica – pica amb pastís d'aniversari inclòs.<br>
											
											• Tenim mini-bubbles d'1,2m per a aniversari d'infantils (entre 6 i 14 anys).<br>

											• Tenim adult-bubbles per a aniversari d'adults (més de 14 anys).<br>

											• Si es desitja contractar una hora addicional s'aplicarà un 25% de descompte a la segona hora. 
											</div>
                                        </div>
                                    </div>
                                    <div class="experience-main experience-main-plus">
                                        <div class="experience-spoiler">
                                            <div class="experience-details-title">
                                                <i class="fa fa-plus"></i>
                                                Lorem ipsum dolor sit amet
                                                <span class="experience-details-collapse"></span>
                                            </div>
                                            <div class="experience-details-content">Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. sit voluptatem accusantium. Doloremque laudantium, totam rem aperiamsit voluptatem accusantium </div>
                                        </div>
                                    </div>
                                    <div class="experience-main experience-main-plus">
                                        <div class="experience-spoiler">
                                            <div class="experience-details-title">
                                                <i class="fa fa-plus"></i>
                                                Lorem ipsum dolor sit amet
                                                <span class="experience-details-collapse"></span>
                                            </div>
                                            <div class="experience-details-content">Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam. sit voluptatem accusantium. Doloremque laudantium, totam rem aperiamsit voluptatem accusantium </div>
                                        </div>
                                    </div>
                                </div>

                            </section>
                        </div>
                        End Training Experience			
 -->															
                    </div>		
                    <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                        <section class="profile">
                            <div class="profile-title">
                                <div class="row">
                                    <div class="img-profile1">
                                        <img src="<?= base_url() ?>img/img-protile.png" alt="">
                                    </div>
                                    <div class="profile-info">
                                        <div class="profile-info-top">
                                            <span>APTE PER A:</span>
                                            <span class="profile-info-right"> TOTS ELS PÚBLICS</span>														
                                        </div>
                                        <div class="profile-info-bottom">
                                            <span>HORA ADICIONAL:</span>
                                            <span class="profile-info-right"> 25% DESCOMPTE</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="profile-content">
                                <div class="profile-icon">
                                    <div class="rating">
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                        <span><i class="fa fa-star"></i></span>
                                    </div>														
                                </div>
                                <div class="profile-text">
                                    <p>
                                    <div class="experience-details-title">
                                                
                                     Què inclou?
                                                
                                            </div>
                                        • Gaudeix del dia amb el millor pack per al teu aniversari!<br>

											• El dia començarà amb un divertit partit i jocs de Bubble Football amb els assistents a la teva gran festa.<br>

											• Al finalitzar us prepararem un pica – pica amb pastís d'aniversari inclòs.<br>
											
											• Tenim mini-bubbles d'1,2m per a aniversari d'infantils (entre 6 i 14 anys).<br>

											• Tenim adult-bubbles per a aniversari d'adults (més de 14 anys).<br>

											• Si es desitja contractar una hora addicional s'aplicarà un 25% de descompte a la segona hora. 
                                    </p>
                                    <div class="share">
                                        <div class="share-title">
                                            <h5>COMPARTIR</h5>										
                                        </div>
                                        <div class="social-icon">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-pinterest"></i></a>
                                            <a href="#" class="in"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>	
                        
                    </div>
                </div>
                <!--End Profile Details-->
                <div class="price-table-title col-md-12 col-sm-12 col-xs-12" style=" color: #222;">
                        <h3>Desde 15'00€ x persona</h3>
                        <h2>Pack Aniversaris</h2>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="price-contents">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="price-tb">
                                    <div class="col-md-12 price-table-content">
                                        <div class="price-table-img">
                                            <img alt="" src="<?= base_url() ?>img/newproducts/nena.png">
                                        </div>
                                        
                                        <div class="price-table-text internal">
                                            <h3>preu per persona</h3>
                                            <h2>De 6 a 14 anys</h2>
                                            <div class="border-bottom"></div>
                                            <p>Tenim mini-bubbles d'1,2 metres per a aniversaris infantils</p>
                                            <div class="price">
                                                <span>15,00€</span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="col-md-12 price-list">
                                        <div class="price-table-1">
                                            <ul>
                                                <li class="icon"><i class="fa fa-calendar-o"></i></li>
                                                <li>1 hora de Bubble</li>
                                            </ul>
                                            <ul>
                                                <li class="icon"><i class="fa fa-clock-o"></i></li>
                                                <li>10 mini-bubbles</li>
                                            </ul>
                                            <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>2 porteries</li>
                                            </ul>	
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>Genolleres</li>
                                            </ul>		
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>música</li>
                                            </ul>	
                                            <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>Vestuaris</li>
                                            </ul>	
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>Assegurança</li>
                                            </ul>	
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>Beguda, pica-pica i pastís</li>
                                            </ul>	
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>De 10 a 20 jugadors</li>
                                            </ul>	
                                             <ul>
                                      
                                                <li>Si es desitja contractar una hora addicional s'aplicarà un 25% de descompte a la segona hora.</li>
                                            </ul>													
                                        </div>
                                        <div class="plan">
                                            <a href="<?= site_url('reserva') ?>">RESERVA partit</a>
                                        </div>										
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="price-tb">
                                    <div class="col-md-12 price-table-content">
                                        <div class="price-table-img">
                                            <img alt="" src="<?= base_url() ?>img/newproducts/teen.png">
                                        </div>
                                        <div class="price-table-text internal">
                                            <h3>preu per persona</h3>
                                            <h2>Més de 14 anys</h2>
                                            <div class="border-bottom"></div>
                                            <p>Gaudeix del dia amb el millor pack per al teu aniversari!</p>
                                            <div class="price">
                                                <span>19,90€</span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="col-md-12  price-list">
                                        <div class="price-table-1">
                                            <ul>
                                                <li class="icon"><i class="fa fa-calendar-o"></i></li>
                                                <li>1 hora de Bubble</li>
                                            </ul>
                                            <ul>
                                                <li class="icon"><i class="fa fa-clock-o"></i></li>
                                                <li>10 adult-bubbles</li>
                                            </ul>
                                            <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>2 porteries</li>
                                            </ul>	
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>Genolleres</li>
                                            </ul>		
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>música</li>
                                            </ul>	
                                            <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>Vestuaris</li>
                                            </ul>	
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>Assegurança</li>
                                            </ul>	
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>Beguda, pica-pica i pastís</li>
                                            </ul>	
                                             <ul>
                                                <li class="icon"><i class="fa fa-leaf"></i></li>
                                                <li>De 10 a 20 jugadors</li>
                                            </ul>	
                                             <ul>
                                      
                                                <li>Si es desitja contractar una hora addicional s'aplicarà un 25% de descompte a la segona hora.</li>
                                            </ul>						
                                        </div>
                                        <div class="plan">
                                               <a href="<?= site_url('reserva') ?>">RESERVA partit</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="col-md-4 col-xs-12 col-sm-12">
								<div class="price-tb">
									<div class="col-md-12 price-table-content" style="height:120px;">
                                        <div class="price-table-img">
                                            <img alt="" src="<?= base_url() ?>img/newproducts/ma.png" style="width: 52.9%;margin-left: 18.8px;margin-top: 5px;">
                                        </div>
                                        <div class="price-table-text internal">
                                            <h2>Puntuació del pack</h2>
                                        </div>
                                    </div>
									<div class="col-md-12  price-list">
										<div class="experience-content">
											<ul>
												<li class="skill skill-top">
													<span class="skill-title">Competitivitat</span>
													<span class="skill-percent skill-percent-callout">95%</span>
													<div data-value="95" class="progress-indicator">
														<div class="bg-red"></div>
													</div>
												</li>
												<li class="skill">
													<span class="skill-title">Diversió</span>
													<span class="skill-percent skill-percent-callout">95%</span>
													<div data-value="95" class="progress-indicator">
														<div class="bg-red"></div>
													</div>
												</li>
												<li class="skill">
													<span class="skill-title">Esforç</span>
													<span class="skill-percent skill-percent-callout">95%</span>
													<div data-value="95" class="progress-indicator">
														<div class="bg-red"></div>
													</div>
												</li>
												<li class="skill">
													<span class="skill-title">Creativitat</span>
													<span class="skill-percent skill-percent-callout">95%</span>
													<div data-value="95" class="progress-indicator">
														<div class="bg-red"></div>
													</div>
												</li>
												<li class="skill">
													<span class="skill-title">Técnica</span>
													<span class="skill-percent skill-percent-callout">95%</span>
													<div data-value="95" class="progress-indicator">
														<div class="bg-red"></div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								
								
								
								<div class="price-tb">
									<div class="col-md-12  price-list" style="padding-left: 15px; margin-top:30px;">
										<div>
											<h4>FORMULARI CONTACTE</h4>
											<div class="headding-bottom"></div>
											<form id="main-contact-form" class="main-contact-form row" name="contact-form" method="post">
												<div class="form-group col-md-12"  style="padding-left:15px">
													<input type="text" name="nom complert" class="control" required="required" placeholder="Nom complert">
												</div>
												<div class="form-group col-md-12">
													<input type="email" name="email" class="control" required="required" placeholder="Email">
												</div>
												<div class="form-group col-md-12">
													<input type="text" name="telèfon" class="control" required="required" placeholder="Telèfon">
												</div>
												<div class="form-group col-md-12">
													<input type="text" name="població" class="control" required="required" placeholder="Població">
												</div>                                
												<div class="row" style="margin-left:0px; margin-right:0px">
													<div class="col-md-12 col-xs-12">
														<div class="form-group col-md-12"  style="padding-left:15px">
															Com t'arribat l'oferta?
															<ul class="list">
																<li><input type='radio' name='oferta' value='Flaix Fm'> Flaix Fm</li>
																<li><input type='radio' name='oferta' value='Radio FlaixBax'> Radio FlaixBax</li>
																<li><input type='radio' name='oferta' value='Prensa'> Premsa</li>
																<li><input type='radio' name='oferta' value='Tv'> Tv</li>
																<li><input type='radio' name='oferta' value='Recomendacion'> Recomanació d'un amic</li>
																<li><input type='radio' name='oferta' value='Otros'> Altres</li>
															</ul>
														</div>
													</div>
													<div class="col-md-12 col-xs-12">
														<div class="form-group col-md-12">
															<textarea name="texto" id="message" required="required" class="control" rows="8" placeholder="Comentari"></textarea>
														</div>
													</div>
												</div>
												<div class="form-group form-submit col-md-12">
													<button type="submit" name="submit" class="btn-submit">Enviar missatge</button>
												</div>
											</form>
										</div>
									</div>
								</div>
								
								
							</div>
            </div>
        </div>
    </div>
    <!-- Main Content -->
</div>
