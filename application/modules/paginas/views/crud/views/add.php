<?php    
    $this->set_js_lib('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
    $this->set_js_config('assets/grocery_crud/themes/bootstrap2/js/flexigrid-add.js');
    $this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
    $this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');
?>
<?php echo form_open( $insert_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
    <div class="row">
        <?php foreach($fields as $field):?>
            <div class="col-md-3">
                <?php echo $input_fields[$field->field_name]->input ?>
            </div>
        <?php  endforeach?>
    </div>    
    <!-- Start of hidden inputs -->
    <?php
            foreach($hidden_fields as $hidden_field){
                    echo $hidden_field->input;
            }
    ?>
    <!-- End of hidden inputs -->    
    <div id='report-error' style="display:none" class='alert alert-danger'></div>
    <div id='report-success' style="display:none" class='alert alert-success'></div>
    <div class="btn-group">			
        <button id="form-button-save" type='submit' class="btn btn-success"><?php echo $this->l('form_save'); ?></button>
        <?php if(!$this->unset_back_to_list) { ?>
        <button type='button' id="save-and-go-back-button"  class="btn btn-default"><?php echo $this->l('form_save_and_go_back'); ?></button>                            
        <button type='button' id="cancel-button"  class="btn btn-danger"><?php echo $this->l('form_cancel'); ?></button>
        <?php } ?>
    </div>                
<?php echo form_close(); ?>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_insert_error = "<?php echo $this->l('insert_error')?>";
                  $("input[type='text'],input[type='password']").addClass('form-control');
</script>