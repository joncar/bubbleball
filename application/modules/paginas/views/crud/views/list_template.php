<?php	        
	$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
	$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');
                 $this->set_js_lib('assets/grocery_crud/js/common/list.js');
                 $this->set_js('assets/grocery_crud/themes/bootstrap2/js/cookies.js');
	$this->set_js('js/crud/main.js');
	$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
	$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
	$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.printElement.min.js');                
                 $this->set_js('assets/grocery_crud/themes/bootstrap2/js/pagination.js');                
	/** Fancybox */
	$this->set_css('assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css');
	$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
	$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');

	/** Jquery UI */
	$this->load_js_jqueryui();
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url();?>';
    var subject = '<?php echo $subject?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>
<div class="container flexigrid">
    <?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
            <div class="row">            
                <div class="col-md-12">
                    <div class="recent-causes style-v1">            
                        <div class="" data-unique-hash="<?php echo $unique_hash; ?>">
                            <div id="hidden-operations" class="hidden-operations"></div>    
                            <div id='ajax_list' class="ajax_list table-responsive">
                                <?php echo $list_view ?>
                            </div>
                        </div>
                    </div><!-- /post -->
                    <!-- /recent-causes -->
                </div><!-- /col-md-12 -->
            </div><!-- /row -->
            <div class="flat-divider d50px"></div>
            <div class="row">
                <div class="button-center">
                    <button class="flat-button button-style" type="button" onclick="document.location.href='<?= site_url('projectes') ?>'">Veure Més</button>
                    <a href="<?= site_url('projectes') ?>" class="hidden">Veure Mes</a>                    
                </div>        
            </div>      
            <input name='page' type="hidden" value="1" size="4" id='page'>
            <input name='per_page' type="hidden" value="3" size="4">
    <?php echo form_close() ?>
</div>
