<!--Banner-->
<section class="page-heading">
    <div class="title-slide">
        <div class="container">
            <div class="banner-content slide-container">									
                <div class="page-title">
                    <h3>Reservar</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Banner-->
<div class="page-content">					
    <!-- Breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <ul>
                        <li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li><span>//</span></li>
                        <li class="category-2"><a href="#">Reservar</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->
    <!-- Our Team -->
    <section id="our-team" class="our-team-page">				
        <div class="our-team-head">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="headding">
                            <div class="headding-title">
                                <h4>Dades sobre nosltres</h4>
                                <div class="headding-bottom"></div>
                            </div>
                            <ul class="headding-content">
                                <li>
                                    <div class="icon-headding">
                                        <i class="fa fa-home"></i>
                                    </div>
                                    <div class="cont-headding">
                                        <h5>Carrer Girona, 34</h5>
                                        <p>08700 IGUALADA. BCN</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-headding">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="cont-headding">
                                        <h5>Web </h5>
                                        <a href="http://bubbleball.cat">http://bubbleball.cat</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon-headding">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <div class="cont-headding">
                                        <h5>Telèfon </h5>
                                        <p>+34 93 803 06 94</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-6 col-xs-12">
                        <div class="headding-title">
                            <h4>Desprès de fer la reserva, recorda!</h4>
                            <div class="headding-bottom"></div>
                        </div>
                        <div class="headding-content">
                            <p>Una vegada feta la reserva et confirmarem si està disponible i ja podràs fer:<br>
                            • TRANFERÈNCIA DE 60€ a ES85 0081 0037 9000 0152 4562 (BANC DE SABADELL)<br>
                            Concepte: RESERVA + EL NOM I COGNOMS DEL TITULAR DE LA RESERVA<br>
                            <br>
                            Quan ens trobem a la pista, al nostre staff organitzador abans de començar a jugar s'ha de pagar l'import restant.
                            
                            
                            </p>
                            <p>
                            <span>PREPAREU-VOS PER GAUDIR AQUESTA EXPERIÈNCIA DEL BUBBLE BALL!!</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Team -->
    <!-- Contact Form -->
    <section class="contact-form">
        <div class="contact-submit">
            <div class="container ">
                <div class="row" style=" margin-top: 30px; margin-bottom: 30px">
                    <?= $output ?>
                </div>
            </div>
        </div>        
    </section>
    <!-- End Contact Form -->
    <!-- Our Partners -->
    <section class="our-partners">
        <div class="container">
            <div class="row">
                <div class="headding-title">
                    <h4>Els nostres col•laboradors</h4>
                    <div class="headding-bottom"></div>
                </div>
                <div class="brand">
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-02.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-03.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-04.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-05.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-06.png" alt=""></div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-12">
                        <div class="img-brand"><img src="<?= base_url() ?>img/logos/home-logo-02.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Partners -->
</div>
