<?php	        
	$this->set_js_lib('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
	$this->set_js_config('assets/grocery_crud/themes/bootstrap2/js/flexigrid-add.js');
	$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
	$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');
?>
<?php echo form_open( $insert_url, ' id="crudForm" method="post" class="row" autocomplete="off" enctype="multipart/form-data"'); ?>
    <div class="col-sm-6 col-xs-12">
        <h4>Formulari de reserva</h4>
        <div class="headding-bottom"></div>                            
        <div class="form-group col-md-12">
            <input name='nombre' type="text" class="control" placeholder="El Teu Nom">
        </div>
        <div class="form-group col-md-12">
            <input name='email' type="email" class="control" placeholder="El Teu Correu">
        </div>
        <div class="form-group col-md-12">
            <input name='telefono' type="text" class="control" placeholder="El Teu Telèfon">
        </div>
        <div class="form-group col-md-12">
            <input type="text" name="fecha" class="control fecha" placeholder="Data Esdeveniment">
        </div>                                               
        <div class="form-group col-md-6">
            <input type="number" name="jugadores" class="control" placeholder="N° Jugadors, Mínim 8" min="8">
        </div>                         
        <div class="form-group col-md-6">
            <select class="form-control" placeholder="Serve A Escouer" name='paquete'>
                <option value="Pack Aniversari 6 A 14 Anys">Pack Aniversari 6 A 14 Anys</option>
                <option value="Pack Aniversari més 14 Anys">Pack Aniversari més 14 Anys</option>                
                <option value="Pack Empresa dinâmica basica 1H">Pack Empresa dinâmica basica 1H</option>
                <option value="Pack Empreses dinâmica Pro 2H">Pack Empreses dinâmica Pro 2H</option>
                <option value="Pack Comiat de Solter Básic">Pack Comiat de Solter Básic</option>
                <option value="Pack Comiat de Solter Premium">Pack Comiat de Solter Premium</option>
                <option value="Pack Club Qualitat">Pack Club Qualitat</option>
                <option value="Pack Club Qualitat & Intes">Pack Club Qualitat & Intes</option>
                <option value="Pack Partit únic bâsic 1h">Pack Partit únic bâsic 1h</option>
                <option value="Pack Partit pro 2h">Pack Partit pro 2h</option>
            </select>
        </div>                                                                                                   
    </div>
    <div class="col-sm-6 col-xs-12">
        <div class="row" style="margin-left:0px; margin-right:0px">
            <div class="col-md-12 col-xs-12">
                <div class="form-group col-md-12">
                    Com ENS HAS CONEGUT?
                    <ul class="list">
                        <li><input type='radio' name='recomendado_por' value='Flaix Fm'> Flaix Fm</li>
                        <li><input type='radio' name='recomendado_por' value='Radio FlaixBax'> Radio FlaixBax</li>
                        <li><input type='radio' name='recomendado_por' value='Prensa'> Premsa</li>
                        <li><input type='radio' name='recomendado_por' value='Tv'> Tv</li>
                        <li><input type='radio' name='recomendado_por' value='Recomendacion'> Recomanació d'un amic</li>
                        <li><input type='radio' name='recomendado_por' value='Otros'> Altres</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="form-group col-md-12">
                    VOLS QUE T'INFORMEM QUAN TINGUEM ALGUNA PROMOCIÓ?
                    <ul class="list">
                        <li><input type='radio' name='boletin' value='1'> SI</li>
                        <li><input type='radio' name='boletin' value='0'> NO</li>
                    </ul>
                </div>
            </div>            
        </div>                           
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="form-group col-md-12">
            <textarea name="observaciones" id="message" class="control" rows="8" placeholder="Observacions"></textarea>
        </div>
    </div>
    <div class="form-group form-submit col-xs-12">
        <button type="submit" class="btn-submit col-xs-12 col-sm-4 col-sm-offset-4">Enviar Reserva</button>
    </div>
    <div class='col-xs-12'>
        <div id='report-error' style="display:none" class='alert alert-danger'></div>
        <div id='report-success' style="display:none" class='alert alert-success'></div>
    </div>
<?php echo form_close(); ?>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_insert_error = "<?php echo $this->l('insert_error')?>";
</script>
