<?php 
    require_once APPPATH.'/controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function reservas($x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_CRUD();  
            $crud->set_table('reservas');
            $crud->set_theme('crud');
            $crud->set_subject('Reservar');
            $crud->set_url('reservas/frontend/reservas/');
            $crud->required_fields_array();
            $crud->set_rules('email','Email','required|valid_email|is_unique[reservas.email]');
            $crud->set_rules('jugadores','Jugadores','required|integer|greater_than[8]');
            $crud->unset_list()
                 ->unset_read()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_edit();
            $crud->callback_after_insert(function($post){
                if(!empty($post['boletin']) && get_instance()->db->get_where('subscritos',array('email'=>$post['email']))->num_rows()==0){
                    get_instance()->db->insert('subscritos',array('email'=>$post['email']));
                }
                get_instance()->enviarcorreo((object)$post,2,'info@bubbleball.cat');
                get_instance()->enviarcorreo((object)$post,3,$post['email']);
            });
            $x = empty($x)?2:'';
            $crud = $crud->render($x,'application/modules/reservas/views/');
            $crud->view = 'reserva';
            $crud->scripts = get_header_crud($crud->css_files, $crud->js_files,TRUE);            
            $this->loadView($crud);
        }
    }
?>
